#!/bin/bash

set -e

SCRIPT="run.sh"

ROOT_DIR=$(git rev-parse --show-toplevel)
source "${ROOT_DIR}/scm/scripts/common/echo.sh"

cd "${ROOT_DIR}/clang/unittest"
./${SCRIPT}

echo_func "[scm] All pass the Unittest"
