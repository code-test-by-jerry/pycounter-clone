#!/bin/bash

set -e

ROOT_DIR=$(git rev-parse --show-toplevel)
source "${ROOT_DIR}/scm/scripts/common/echo.sh"

# database
cd "${ROOT_DIR}/db"
./run_unittest.sh

# TODO: gui

echo_func "[scm] All pass the Unittest."
