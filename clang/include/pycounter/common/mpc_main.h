/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:ffs=unix */
#ifndef __MPC_MAIN_H__
#define __MPC_MAIN_H__

int mpc_main_init(void);
int mpc_main_exit(void);

#endif /* __MPC_MAIN_H__ */
