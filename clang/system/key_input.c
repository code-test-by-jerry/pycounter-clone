#include <stdint.h>
#include <stdlib.h>

#include <pycounter/common/mpc_common.h>
#include <pycounter/system/key_input.h>

#define INPUT_PTH_GUARD(ig)                                                    \
    do                                                                         \
    {                                                                          \
        if (unlikely(ig < 0))                                                  \
        {                                                                      \
            ERR_MSG("Error input pthread");                                    \
            return key_input_exit(), NULL;                                     \
        }                                                                      \
    } while (0)

typedef struct thread_information
{
    sync_t sync;

} pth_info_t;

static pth_info_t *_key_input_create(void);
static int _key_input_destroy(pth_info_t *pth_info);

static pth_info_t *g_keyinput;

int key_input_init(void)
{
    // create input pthread
    g_keyinput = _key_input_create();

    if (g_keyinput == NULL)
    {
        ERR_MSG("Failed to create key input");
        return key_input_exit(), -EPERM;
    }

    return 0;
}

int key_input_exit(void)
{
    _key_input_destroy(g_keyinput);

    return 0;
}

static pth_info_t *_key_input_create(void)
{
    pth_info_t *pth_info = calloc(1, sizeof(pth_info_t));

    if (pth_info == NULL)
    {
        ERR_MSG("Failed to allocate the memory.");
        return NULL;
    }

    // set input pthread mutex
    int ret = pthread_cond_init(&pth_info->sync.cv, NULL);
    INPUT_PTH_GUARD(ret);
    ret = pthread_mutex_init(&pth_info->sync.lock, NULL);
    INPUT_PTH_GUARD(ret);

    return pth_info;
}

static int _key_input_destroy(pth_info_t *pth_info)
{
    FORMULA_GUARD(pth_info == NULL, -EPERM, ERR_INVALID_PTR);

    // quit input pthread mutex
    pthread_cond_destroy(&pth_info->sync.cv);
    pthread_mutex_destroy(&pth_info->sync.lock);

    // memory free
    PTR_FREE(pth_info);

    return 0;
}
