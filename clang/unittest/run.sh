#!/bin/bash

UNITTEST_DIR=$(pwd)
CMAKE_TXT="CMakeLists.txt"

BUILD_LOG="build.log"
MEM_STATUS="total heap usage: (.*) allocs, (.*) frees, (.*) bytes allocated"

DIRS=(common)

finish()
{
	cd "${UNITTEST_DIR}"
	rm -rf out

	for DIR in "${DIRS[@]}"
	do
		find ./"${DIR}" -name "${CMAKE_TXT}" -exec rm {} \;
	done

	echo "Unittest Clean!"
}
trap finish INT EXIT QUIT TERM

set -e

# setup the CMakeLists.txt for build
for DIR in "${DIRS[@]}"
do
	cp "${CMAKE_TXT}_parent" "${DIR}/${CMAKE_TXT}"

	cd "${DIR}"

	PARENT=$(find . -maxdepth 1 -type d -not -path '\.' -exec basename {} \;)
	for CHILD in ${PARENT}
	do
		cp "../${CMAKE_TXT}_child" "${CHILD}/${CMAKE_TXT}"
	done

	cd "${UNITTEST_DIR}"
done

# build for unittest
rm -rf out && mkdir -p out && cd out && cmake ../ && make

# execute the unittest
for DIR in "${DIRS[@]}"
do
	cd "${DIR}"

	PARENT=$(find . -maxdepth 1 -type d -not -path '\.' -exec basename {} \;)
	for CHILD in ${PARENT}
	do
		if [[ ! ${CHILD} =~ "mpc_" ]]
		then
			continue
		fi

		"${CHILD}/unittest_${CHILD}"

		# memory leak check
		if [ -f "/usr/bin/valgrind" ]
		then
			valgrind \
				--leak-check=full \
				--show-leak-kinds=all \
				"${CHILD}/unittest_${CHILD}" > "${BUILD_LOG}" 2>&1

			ret=$(grep "total heap usage" < ${BUILD_LOG} | \
				awk -F '==   ' '{print $2}')

			[[ ${ret} =~ ${MEM_STATUS} ]]

			if [[ "${BASH_REMATCH[1]}" != "${BASH_REMATCH[2]}" ]]
			then
				echo -e "\033[0;31m${ret}"
				echo -e "Memory leak occurred! Please refer to the \
					${BUILD_LOG}\033[0m"
				exit 1
			fi
		fi

		# clean
		rm -rf ${BUILD_LOG}
	done

	cd "${UNITTEST_DIR}/out"
done
