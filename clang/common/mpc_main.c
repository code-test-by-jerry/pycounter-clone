#include <pycounter/common/mpc_common.h>
#include <pycounter/common/mpc_json.h>
#include <pycounter/common/mpc_main.h>
#include <pycounter/common/mpc_pipe.h>
#include <pycounter/system/key_input.h>
#include <pycounter/timer/mpc_timer.h>

static void _mpc_ctrl_c(int signo);

static int g_escape_flag = 1;

static void _mpc_ctrl_c(int signo)
{
    (void)signo;

    ERR_MSG("Input the \"Ctrl+C\".");
    g_escape_flag = 0;
}

int mpc_main_init(void)
{
    /* Only root! */
    FORMULA_GUARD(check_permission(NULL) < 0, -EPERM, "");

    /* start pipe */
    FORMULA_GUARD(mpc_pipe_init() < 0, -EPERM, "");

    /* start json */
    FORMULA_GUARD(mpc_json_init() < 0, -EPERM, "");

    /* start timer */
    FORMULA_GUARD(mpc_timer_init() < 0, -EPERM, "");

    /* start key input */
    FORMULA_GUARD(key_input_init() < 0, -EPERM, "");

    return 0;
}

int mpc_main_exit(void)
{
    /* stop key input */
    key_input_exit();

    /* stop timer */
    mpc_timer_exit();

    /* stop json */
    mpc_json_exit();

    /* stop pipe */
    mpc_pipe_exit();

    return 0;
}

#ifdef _STANDALONE_VERSION
int main(void)
{
    struct sigaction sig_act = {
        .sa_handler = _mpc_ctrl_c,
        .sa_flags = 0,
    };
    sigemptyset(&sig_act.sa_mask);
    sigaction(SIGINT, &sig_act, NULL);

    int ret = mpc_main_init();
    if (ret < 0)
        return mpc_main_exit(), -EPERM;

    while (g_escape_flag)
    {
        sleep(1);
    }

    mpc_main_exit();
    return 0;
}
#endif
