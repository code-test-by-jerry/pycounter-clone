#!/bin/bash

ROOT_DIR=$(git rev-parse --show-toplevel)
source "${ROOT_DIR}/scm/scripts/common/echo.sh"

CLANG_DIR=${ROOT_DIR}/clang
OUT_DIR=${CLANG_DIR}/out
JSON_OUT_DIR=${OUT_DIR}/json-c
CMOCKA_OUT_DIR=${OUT_DIR}/cmocka

CPU_NUM=$(grep -c processor /proc/cpuinfo)

# check the output directory
function create_dir()
{
	if [ ! -d "$1" ]
	then
		echo_func "[clang] Created the directory $1" 0
		mkdir "$1"
	fi
}

function check_result()
{
	if [ ! -d "lib" ] || [ ! -d "include" ]
	then
		echo_func "[clang err] Failed to build the json-c" 1
		exit 1
	fi
}

function do_build()
{
	make clean
	make -j "${CPU_NUM}"
	make install

	check_result
}

function build_for_json()
{
	# setup the environment
	cd "${JSON_OUT_DIR}" || exit 1
	if [ ! -f "Makefile" ]
	then
		"${CLANG_DIR}"/third_party/json-c/cmake-configure \
			--prefix="${JSON_OUT_DIR}"
	fi

	# build
	do_build
}

function build_for_cmocka()
{
	# setup the environment
	cd "${CMOCKA_OUT_DIR}" || exit 1
	cmake \
		-DCMAKE_INSTALL_PREFIX="${CMOCKA_OUT_DIR}" \
		-DCMAKE_BUILD_TYPE=Debug \
		"${CLANG_DIR}"/third_party/cmocka

	# build
	do_build
}

function build_for_clang()
{
	cd "${OUT_DIR}" || exit 1

	# build
	echo_func "[clang] Build Start!" 0
	cmake ../
	make 2>&1 | tee build.log

	ret=$(grep -e " warning:" -e " error:" < build.log)
	if [[ ${ret} != "" ]]
	then
		echo_func "[clang err] Occured the warning/error when compile time" 1
		rm -rf build.log
		exit 1
	fi

	rm -rf build.log
}

# remove out directory
rm -rf "${OUT_DIR}"

# check the output directory
create_dir "${OUT_DIR}"
create_dir "${JSON_OUT_DIR}"
create_dir "${CMOCKA_OUT_DIR}"

# build json-c
build_for_json

# build cmocka
build_for_cmocka

# build the clang
build_for_clang

echo_func "[clang] Build Done!" 0
