#!/bin/bash

ROOT_DIR=$(git rev-parse --show-toplevel)
DATABASE_DIR=${ROOT_DIR}/db
PRELIMINARY_DB="json_db/pre_round_db.json"

source "${DATABASE_DIR}/.profile"

RET="$?"
if [[ "${RET}" -ne "0" ]]
then
	echo "Failed to set envrionment!"
	exit 1
fi
echo "[mpc_gui] Success to set envrionment for python"

export DISPLAY=:0
python3 mpc_gui.py "${DATABASE_DIR}" "${PRELIMINARY_DB}"
