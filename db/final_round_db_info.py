#!/usr/bin/env python3
"""This is the final round db API for the GUI"""

import sys

from preliminary_round_db_info import GetUserInfo

###############################################################################
#  GetFinalUserInfo class


class GetFinalUserInfo(GetUserInfo):
    """Class for the final round db API for the GUI"""
    def __init__(self, abs_path: str, json_path: str):
        try:
            super().__init__(abs_path, json_path)

            # re-set the value for the final round db
            group_len = len(self._json.dct_group("A"))
            if group_len != 0:
                user = self._json.dct_group("A")
            else:
                user = self._json.step_group("A")

            self._json.fresh = user
            self._grp_table = [user]
            self._group_limit = 1
        except AttributeError:
            print("Failed to init the final_round_db_info.py")
            return

    ###########################################################################
    # Public methods
    def get_db_len(self) -> int:
        """ return all database """
        try:
            return len(self._json.db_all)
        except AttributeError:
            return -1

    ###########################################################################
    # Internal methods


def main() -> None:
    """main"""
    # step database for final round match
    step = GetFinalUserInfo("./", "json_db/Step_final_round_db.json")

    print(step.first_group())
    print("")
    for _ in range(0, step.get_db_len()):
        print(step.next_player())
        print("")

    print("")

    # DC database for final match
    dct = GetFinalUserInfo("./", "json_db/DC_final_round_db.json")

    print(dct.first_group())
    print("")
    for _ in range(0, dct.get_db_len()):
        print(dct.next_player())
        print("")

    step.save_database("./json_db", "Step_Main")
    dct.save_database("./json_db", "DC_Main")


if __name__ == "__main__":
    sys.exit(main() or 0)
