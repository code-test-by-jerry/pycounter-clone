#!/bin/bash

set -e

ROOT_DIR=$(git rev-parse --show-toplevel)
source "${ROOT_DIR}/scm/scripts/common/echo.sh"

test_files=$(find unit_test/* -name "test_*.py")
for file in ${test_files}
do
	python3 -m unittest -v "${file}"

	RET="$?"
	if [[ "${RET}" -ne "0" ]]
	then
		echo_func "[unittest] Failed the Unit Test" 1
		exit 1
	fi
done

echo_func "[unittest] Completed the Unit Test!" 0
