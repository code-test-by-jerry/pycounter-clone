#!/usr/bin/env python3
"""This is the python file for the setting group in the json"""

import json
import os
import string
import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QFileDialog, QMainWindow, QMessageBox

from config.config_parser import GetConfigInfo

###############################################################################
# SetGroup class

CUSTOM_UI = uic.loadUiType("set_group.ui")[0]


class SetGroup(QMainWindow, CUSTOM_UI):
    """Class for the setting group in the json"""
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        # set title
        self.setWindowTitle("Setting Group")

        # set push buttion
        self.file_open.clicked.connect(self.file_open_clicked)
        self.group_save.clicked.connect(self.group_save_clicked)

        # internal variables
        self.__db_path = None
        self.__db_all = None
        self.__cfg = GetConfigInfo(os.getcwd()[:-4] + "config/group.cfg")

    def __del__(self) -> None:
        del self.__cfg
        del self.__db_all

    ###########################################################################
    # Public methods
    def file_open_clicked(self) -> None:
        """When file open button clicked,"""
        self.__db_path = QFileDialog.getOpenFileName()[0]

        # get json db
        if self.__check_format(self.__db_path[-4:]) is True:
            # set text
            self.label.setText(self.__db_path)

            # get json
            self.__get_json_db(self.__db_path)

            # load to combobox
            self.__add_user_on_ui()

    def group_save_clicked(self) -> None:
        """When save button clicked"""
        if self.user_combobox.count() == 0:
            self.label.setText("Need to opend file!")
            return

        # get user info
        user_idx = int(self.user_combobox.currentText().split(",")[0])

        # save in the db
        self.__db_all[user_idx]["group"] = self.group_combobox.currentText()

        # save database
        with open(self.__db_path, "w") as save_db:
            json.dump(self.__db_all, save_db, indent=4, ensure_ascii=False)

        # run the popup
        QMessageBox.about(self, "Popup", "Saved")

    ###########################################################################
    # Internal methods
    def __check_format(self, file_format: str) -> bool:
        if file_format != "json":
            self.label.setText("Only select the json file format!")
            self.__db_all, self.__db_path = None, None
            self.user_combobox.clear()
            self.group_combobox.clear()
            return False

        # set the click event
        self.user_combobox.activated[str].connect(self.__add_group_on_ui)
        return True

    def __get_json_db(self, path: str) -> None:
        with open(path) as read_file:
            self.__db_all = json.load(read_file)

    def __add_user_on_ui(self) -> None:
        for idx, user in enumerate(self.__db_all):
            name = user["name"]
            affil = user["affiliation"]
            robot = user["robot"]

            info = str(idx)
            if "DC " in user["field"]:
                info += ", DC, "
            elif "Step" in user["field"]:
                info += ", Step, "
            else:
                continue

            info += name + ", " + affil + ", " + robot
            self.user_combobox.addItem(info)

        # init
        self.__add_group_on_ui(self.user_combobox.currentText())

    def __add_group_on_ui(self, cur_text: str) -> None:
        # init group combobox
        self.group_combobox.clear()
        if self.user_combobox.count():
            if "DC" in cur_text:
                group_idx = self.__cfg.get_dc_group_num
            elif "Step" in cur_text:
                group_idx = self.__cfg.get_step_group_num

            for idx in range(group_idx):
                self.group_combobox.addItem(string.ascii_uppercase[idx])

            # get the group info from db
            user_idx = int(cur_text.split(",")[0])
            group = self.__db_all[user_idx]["group"]
            self.group_combobox.setCurrentIndex(ord(group) - ord("A"))


def main() -> None:
    """main"""
    app = QApplication(sys.argv)
    set_group = SetGroup()
    set_group.show()
    app.exec_()


if __name__ == "__main__":
    sys.exit(main() or 0)
