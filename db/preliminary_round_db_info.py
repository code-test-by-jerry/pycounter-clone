#!/usr/bin/env python3
# pylint: disable=too-many-instance-attributes
"""This is the DB API for the GUI"""

import sys
from typing import Generator

from config.config_parser import GetConfigInfo
from json_db.json_parser import JsonParser
from json_db.json_saver import JsonSaver

###############################################################################
#  GetUserInfo class


class GetUserInfo:
    """Class for the DB API for the GUI"""

    CONFIG = "config/group.cfg"

    def __init__(self, abs_path: str, json_path: str) -> None:
        try:
            # Deal the path
            if abs_path[-1] != "/":
                abs_path += "/"

            self._json = JsonParser(abs_path + GetUserInfo.CONFIG,
                                    abs_path + json_path)
            self.__saver = JsonSaver()
            self.__player_idx, self.__group_idx = 0, 0
            self.__cur_group = []

            # set the group table
            cfg = GetConfigInfo(abs_path + GetUserInfo.CONFIG)
            self._group_limit = cfg.get_dc_group_num + cfg.get_step_group_num
            self._group_limit += 2  # fresh, mouse

            self._grp_table = [self._json.fresh]
            for idx in range(0, cfg.get_step_group_num):
                __idx = chr(idx + ord("A"))
                self._grp_table.append(self._json.step_group(__idx))

            for idx in range(0, cfg.get_dc_group_num):
                __idx = chr(idx + ord("A"))
                self._grp_table.append(self._json.dct_group(__idx))

            self._grp_table.append(self._json.mouse)
            del cfg

            self.__preliminary = True
        except IndexError:
            print("Failed to init the preliminary_round_db_info.py")
            self.__preliminary = False

    ###########################################################################
    # Public methods
    def before_player(self) -> tuple:
        """When input the \'key_b\'"""
        if self.__preliminary is False:
            return ()

        self.__player_idx -= 1
        return self.current_player()

    def next_player(self) -> tuple:
        """When input the \'key_n\'"""
        if self.__preliminary is False:
            return ()

        self.__player_idx += 1
        return self.current_player()

    def current_player(self) -> tuple:
        """Return information of the current player"""
        if self.__preliminary is False:
            return ()

        if self.__player_idx < 0:
            self.__player_idx = 0
        elif self.__player_idx >= len(self.__cur_group):
            self.__player_idx = len(self.__cur_group) - 1

        return self.__get_information(self.__player_idx)

    def before_group(self) -> tuple:
        """When input the \'key_f\'"""
        if self.__preliminary is False:
            return ()

        self.__group_idx -= 1
        if self.__group_idx < 0:
            self.__group_idx = 0

        self.__cur_group = self._grp_table[self.__group_idx]
        self.__player_idx = 0
        return self.__get_information(0)

    def next_group(self) -> tuple:
        """When input the \'key_g\'"""
        if self.__preliminary is False:
            return ()

        self.__group_idx += 1
        if self.__group_idx >= self._group_limit:
            self.__group_idx = self._group_limit - 1

        self.__cur_group = self._grp_table[self.__group_idx]
        self.__player_idx = 0
        return self.__get_information(0)

    def first_group(self) -> tuple:
        """When set the first group"""
        if self.__preliminary is False:
            return ()

        self.__group_idx = 0
        self.__cur_group = self._json.fresh
        self.__player_idx = 0
        return self.__get_information(0)

    def manage_record(self, player: dict, record: float, mode: str) -> bool:
        """
        For managing the record
        player -> {"name": "", "affiliation": "", "robot": ""}
        """
        if self.__preliminary is False:
            return False

        if not (mode == "add" or mode == "del"):
            return False

        user_idx = self.__find_user(player)
        if user_idx == -1:
            return False

        if mode == "add":
            self._json.db_all[user_idx]["record"].append(record)
        elif mode == "del":
            try:
                rec_idx = self._json.db_all[user_idx]["record"].index(record)
                del self._json.db_all[user_idx]["record"][rec_idx]
            except ValueError:
                print("Not find to match user information.")
                return False
        return True

    def save_database(self, path: str, mode: str) -> bool:
        """Saving the database"""
        if self.__preliminary is False:
            return False

        return self.__saver.save_db_file(self._json.db_all,
                                         path=path,
                                         mode=mode)

    ###########################################################################
    # Internal methods
    def __get_information(self, player_idx: int) -> tuple:
        ulen = len(self.__cur_group)
        if player_idx >= ulen or player_idx < 0:
            return ()

        info = self.__user_info(player_idx)
        ulist = self.__user_list(player_idx)
        record = self.__user_record(player_idx)
        rank = self.__user_rank()
        return (info, list(ulist), record, rank)

    def __user_info(self, idx: int) -> tuple:
        name = self.__cur_group[idx]["name"]
        affil = self.__cur_group[idx]["affiliation"]
        robot = self.__cur_group[idx]["robot"]
        return (name, affil, robot)

    def __user_list(self, user_idx: int) -> Generator[list, None, None]:
        cnt = 0
        for user in self.__cur_group:
            cnt += 1
            if user_idx >= cnt:
                continue

            yield [user["name"], user["affiliation"]]

    def __user_record(self, idx: int) -> list:
        record = self.__cur_group[idx]["record"]
        rlen = len(record)
        return [] if rlen == 0 else record

    def __user_rank(self) -> list:
        rank = []
        for user in self.__cur_group:
            rlen = len(user["record"])
            if rlen:
                sort = sorted(user["record"])[0]
                rank.append((user["name"], user["affiliation"], sort))

        # sort as record data
        rank.sort(key=lambda x: x[2])
        return rank

    def __find_user(self, player: dict) -> int:
        if "name" not in player or \
                "affiliation" not in player or "robot" not in player:
            return -1

        # find the matched user in the db
        find_idx = -1
        for user_idx, user in enumerate(self._json.db_all):
            if player["name"] != user["name"]:
                continue

            if player["affiliation"] != user["affiliation"]:
                continue

            if player["robot"] != user["robot"]:
                continue

            find_idx = user_idx
            break

        return find_idx


def main():
    """main"""
    user = GetUserInfo("./", "json_db/pre_round_db.json")
    cfg = GetConfigInfo("./config/group.cfg")

    # fresh man
    print(user.first_group())

    # add record test
    user_info_1 = {}
    user_info_1["name"] = "이재성"
    user_info_1["affiliation"] = "단국대학교 MAZE"
    user_info_1["robot"] = "_varhae_step_"
    user.manage_record(user_info_1, 15.232, "add")
    user.manage_record(user_info_1, 11.09, "add")
    user.manage_record(user_info_1, 8.734, "add")
    user.manage_record(user_info_1, 9.345, "add")

    print(user.next_player())

    # add record test
    user_info_2 = {}
    user_info_2["name"] = "이재성"
    user_info_2["affiliation"] = "단국대학교 MAZE"
    user_info_2["robot"] = "_VarHae_"
    user.manage_record(user_info_2, 6.345, "add")
    user.manage_record(user_info_2, 7.123, "add")

    print(user.next_player())

    # del record test
    user.manage_record(user_info_1, 8.734, "del")
    print(user.current_player())

    # Step group
    for _ in range(0, cfg.get_step_group_num):
        print(user.next_group())

    # DC group
    for _ in range(0, cfg.get_dc_group_num):
        print(user.next_group())

    # Micro Mouse
    print(user.next_group())

    # add record test
    user_info_1 = {}
    user_info_1["name"] = "엄상훈, 이재성"
    user_info_1["affiliation"] = "단국대학교 MAZE"
    user_info_1["robot"] = "_H2_"
    user.manage_record(user_info_1, 15.232, "add")
    user.manage_record(user_info_1, 11.09, "add")
    user.manage_record(user_info_1, 8.734, "add")
    user.manage_record(user_info_1, 9.345, "add")

    print(user.next_player())

    # add record test
    user_info_2 = {}
    user_info_2["name"] = "김건희"
    user_info_2["affiliation"] = "고려대학교 CMLLAB"
    user_info_2["robot"] = "_boy_"
    user.manage_record(user_info_2, 6.345, "add")
    user.manage_record(user_info_2, 7.123, "add")

    # create the result csv file
    user.save_database("./json_db", "fresh")
    user.save_database("./json_db", "mouse")

    # create the final db json file
    user.save_database("./json_db", "Step")
    user.save_database("./json_db", "DC")


if __name__ == "__main__":
    sys.exit(main() or 0)
