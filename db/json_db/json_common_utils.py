# pylint: disable=bad-continuation
"""This is the python file for common in the json db"""

import os

from config.config_parser import GetConfigInfo


def set_group_info(cfg_path: str, step: list, dct: list) -> bool:
    """For setting the group information when init"""

    if not os.path.exists(cfg_path):
        print("[ERR] Failed to find the file config file path")
        return False

    cfg = GetConfigInfo(cfg_path)
    if cfg.get_dc_group_num == -1 or cfg.get_step_group_num == -1:
        return False

    for _ in range(0, cfg.get_dc_group_num):
        dct.append([])

    for _ in range(0, cfg.get_step_group_num):
        step.append([])

    del cfg
    return True


def check_field(raw_db: list, fresh: list, step: list, dct: list,
                mouse: list) -> None:
    """Check the each field in the raw database"""

    for db_list in raw_db:
        if db_list["field"] == "Micro Mouse":
            mouse.append(db_list)
        else:
            if "DC" in db_list["field"]:
                idx = get_idx_in_group(db_list["group"])
                if idx != -1 and idx < len(dct):
                    dct[idx].append(db_list)
            elif "Step" in db_list["field"]:
                idx = get_idx_in_group(db_list["group"])
                if idx != -1 and idx < len(step):
                    step[idx].append(db_list)
            else:
                fresh.append(db_list)


def get_idx_in_group(group: str) -> int:
    """Return the matched index in the group"""
    if len(group) != 1:
        return -1

    idx = ord(group) - ord("A")
    return idx if idx >= 0 else -1
