#!/usr/bin/env python3
""" This is the unittest file for the config_saver """
# pylint: disable=no-value-for-parameter, too-many-function-args, missing-kwoa

import os
import unittest
from configparser import ConfigParser

from config.config_saver import SetConfigInfo

###############################################################################
# TestSetConfigInfo class


class TestSetConfigInfo(unittest.TestCase):
    """ Class for the unittest """

    TEMP_FILE = "./temp.cfg"
    TEST_COUNT_1 = "3"
    TEST_COUNT_2 = "7"

    def setUp(self) -> None:
        self.__cfg = ConfigParser()
        self.__cfg["group"] = {}
        self.__cfg["group"]["dc"] = TestSetConfigInfo.TEST_COUNT_1
        self.__cfg["group"]["step"] = TestSetConfigInfo.TEST_COUNT_2

        with open(TestSetConfigInfo.TEMP_FILE, "w", encoding="utf-8") as file:
            self.__cfg.write(file)

        self.__class = SetConfigInfo(TestSetConfigInfo.TEMP_FILE)

    def tearDown(self) -> None:
        os.remove(TestSetConfigInfo.TEMP_FILE)
        del self.__class

    ###########################################################################
    # Unittest methods

    def test_class(self) -> None:
        """ Unit Test for the class """
        with self.assertRaises(TypeError):
            SetConfigInfo([])

        with self.assertRaises(TypeError):
            SetConfigInfo()

    def test_save_config_file(self) -> None:
        """ Unit Test for the save_config_file """
        with self.assertRaises(TypeError):
            tc_1 = SetConfigInfo(-1)
            print(tc_1.save_config_file(0, 0))

        self.assertEqual(tc_1.save_config_file(dc_num=0, step_num=0), False)

        tc_2 = SetConfigInfo("")
        self.assertEqual(tc_2.save_config_file(dc_num=0, step_num=0), False)

        self.assertEqual(self.__class.save_config_file(dc_num=1, step_num=1),
                         True)

    def test_get_config_value(self) -> None:
        """ Unit Test for the get_config_value """
        tc_1 = SetConfigInfo(-1)
        self.assertEqual(tc_1.get_config_value(), (-1, -1))

        self.assertEqual(
            self.__class.get_config_value(),
            (TestSetConfigInfo.TEST_COUNT_1, TestSetConfigInfo.TEST_COUNT_2),
        )


if __name__ == "__main__":
    unittest.main()
