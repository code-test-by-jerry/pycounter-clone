#!/usr/bin/env python3
# pylint: disable=too-many-function-args, missing-kwoa
"""This is the unittest file for the json_saver"""

import os
import unittest

from json_db.json_saver import JsonSaver

###############################################################################
# TestJsonSaver class


class TestJsonSaver(unittest.TestCase):
    """Class for the unittest"""
    def setUp(self) -> None:
        self.__json = JsonSaver()

    def tearDown(self) -> None:
        pass

    ###########################################################################
    # Unittest methods
    def test_class(self) -> None:
        """Unit Test for the class"""
        with self.assertRaises(TypeError):
            JsonSaver("")

    def test_save_db_file(self) -> None:
        """Unit Test for the save_db_file"""
        with self.assertRaises(TypeError):
            self.__json.save_db_file([], "./", "mouse")

        self.assertFalse(self.__json.save_db_file([], path="", mode=""))

        user_info = {}
        user_info["field"] = "Micro Mouse"
        user_info["name"] = "엄상훈, 이재성"
        user_info["affiliation"] = "단국대학교 MAZE"
        user_info["robot"] = "_H2_"
        user_info["group"] = "A"
        user_info["record"] = [1.23, 4.56]

        self.__json.save_db_file([user_info], path="./", mode="mouse")
        self.assertTrue(os.path.exists("./mouse_result_db.csv"))
        os.remove("./mouse_result_db.csv")

    def test_print_db_file(self) -> None:
        """Unit Test for the print_db_file"""
        self.assertFalse(self.__json.print_db_file())


if __name__ == "__main__":
    unittest.main()
