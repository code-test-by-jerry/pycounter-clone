#!/usr/bin/env python3
"""This is the unittest file for the final_round_db_info"""

import unittest

from final_round_db_info import GetFinalUserInfo

###############################################################################
# TestGetFinalUserInfo class


class TestGetFinalUserInfo(unittest.TestCase):
    """Class for the unittest"""
    def setUp(self) -> None:
        self.__dc = GetFinalUserInfo("./", "json_db/DC_final_round_db.json")
        self.__step = GetFinalUserInfo("./",
                                       "json_db/Step_final_round_db.json")

    def tearDown(self) -> None:
        del self.__dc
        del self.__step

    ###########################################################################
    # Unittest methods
    def test_class(self) -> None:
        """Unit Test for the class"""
        tc_dc = GetFinalUserInfo([], ())
        tc_step = GetFinalUserInfo("", "")

        self.assertIsInstance(tc_dc, GetFinalUserInfo)
        self.assertIsInstance(tc_step, GetFinalUserInfo)

    def test_get_db_len(self) -> None:
        """Unit Test for the get_db_len"""
        tc_dc = GetFinalUserInfo([], ())
        tc_step = GetFinalUserInfo("", "")

        self.assertEqual(tc_dc.get_db_len(), -1)
        self.assertEqual(tc_step.get_db_len(), -1)

        self.assertNotEqual(self.__dc.get_db_len(), -1)
        self.assertNotEqual(self.__step.get_db_len(), -1)


if __name__ == "__main__":
    unittest.main()
